
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Application {

	
	public static void main(String[] args) throws IOException{
		if (args.length == 0) {
			System.err.println("Provide an input file for processing");
			return;
		}
		
		FileReader fileReader = new FileReader(args[0]);
		BufferedReader br = new BufferedReader(fileReader);
		try {
			String line;
			HashMap<String, Integer> hashmap = new HashMap<String, Integer>();  
			while ((line = br.readLine()) != null)
			{
 				String[] columns = line.split(",");
                        	if(columns != null && columns.length > 0 && !columns[0].equalsIgnoreCase("C_YEAR")) {   // ignore first line
                                	String year = columns[0];
                                	String severity = columns[4];
                                	String key = year.concat("-").concat(severity);
					if(hashmap.get(key) == null) {
						hashmap.put(key, 1);
					} else {
						int count = hashmap.get(key);
						hashmap.put(key, ++count);
					}
                        	}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.toString());
		}
	}	
}

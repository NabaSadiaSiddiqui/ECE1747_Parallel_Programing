#!/usr/bin/python

import pandas as pd
import sys

if len(sys.argv) <= 1:
	print "Provide an input file for processing. Exiting now..."
else:
	data = pd.read_csv(sys.argv[1], usecols = ['C_YEAR', 'C_SEV'], dtype={'C_YEAR': str, 'C_SEV': str})
	print data.groupby(['C_YEAR', 'C_SEV']).size()

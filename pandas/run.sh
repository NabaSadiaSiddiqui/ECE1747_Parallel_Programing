#!/bin/bash

ls -lsh ../data/NCDB_1999_to_2012.csv
echo "Running it against NCDB_1999_to_2012.csv"
time python app.py ../data/NCDB_1999_to_2012.csv

cd ../data
cp NCDB_1999_to_2012.csv NCDB_1999_to_2012_3.csv
cat NCDB_1999_to_2012* > NCDB_1999_to_2012_2.csv
rm NCDB_1999_to_2012_3.csv
cd ../pandas
ls -lsh ../data/NCDB_1999_to_2012_2.csv
echo "Running it against NCDB_1999_to_2012_2.csv"
time python app.py ../data/NCDB_1999_to_2012_2.csv

cd ../data
cat NCDB_1999_to_2012* > NCDB_1999_to_2012_3.csv
cd ../pandas
ls -lsh ../data/NCDB_1999_to_2012_3.csv
echo "Running it against NCDB_1999_to_2012_3.csv"
time python app.py ../data/NCDB_1999_to_2012_3.csv

cd ../data
cat NCDB_1999_to_2012* > NCDB_1999_to_2012_4.csv
cd ../pandas
ls -lsh ../data/NCDB_1999_to_2012_4.csv
echo "Running it against NCDB_1999_to_2012_4.csv"
time python app.py ../data/NCDB_1999_to_2012_4.csv

cd ../data
cat NCDB_1999_to_2012* > NCDB_1999_to_2012_5.csv
cd ../pandas
ls -lsh ../data/NCDB_1999_to_2012_5.csv
echo "Running it against NCDB_1999_to_2012_5.csv"
time python app.py ../data/NCDB_1999_to_2012_5.csv

cd ../data
cat NCDB_1999_to_2012* > NCDB_1999_to_2012_6.csv
cd ../pandas
ls -lsh ../data/NCDB_1999_to_2012_6.csv
echo "Running it against NCDB_1999_to_2012_6.csv"
time python app.py ../data/NCDB_1999_to_2012_6.csv

cd ../data
cat NCDB_1999_to_2012* > NCDB_1999_to_2012_7.csv
cd ../pandas
ls -lsh ../data/NCDB_1999_to_2012_7.csv
echo "Running it against NCDB_1999_to_2012_7.csv"
time python app.py ../data/NCDB_1999_to_2012_7.csv

#!/bin/bash

# Start Hadoop
$HADOOP_HOME/sbin/start-dfs.sh
$HADOOP_HOME/sbin/start-yarn.sh

# Create input directory in HDFS (/var/app/hadoop/data) and put a file there
hadoop fs -mkdir -p /var/app/hadoop/data/input
hadoop fs -put /home/hadoop/ECE1747_Parallel_Programing/data/NCDB_1999_to_2012.csv /var/app/hadoop/data/input

# Run MapReduce job
time hadoop jar application.jar Application /var/app/hadoop/data/input/NCDB_1999_to_2012.csv /var/app/hadoop/data/output
